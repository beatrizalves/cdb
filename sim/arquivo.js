let times = [];
class Time {
  constructor(nome, cidadeSede,id, titulosNaCompeticao,divisao) {
    this.nome = nome;
    this.cidadeSede = cidadeSede;
    this.id = id;
    this.titulosNaCompeticao = titulosNaCompeticao;
    this.divisao = divisao;
  }
}

function armazenar() {
  let nome = byId("nomeTime").value;
  let time= new Time(nome);
  times.push(time);
  placares();
}

function montarOpcoes() {
  let opcoes = "<select>"
  for(let time of times) {
    opcoes += "<options>" + time+"</options>";
  }
  opcoes += "<select/>";
  return opcoes
    }

function montarTabela() {
    let jogo = montarOpcoes()
    const input = '<input type = "text">';
    jogo += input + "X"+ input;
    jogo += montarOpcoes();
    return jogo
}

function byId(id) {
  return document.getElementById(id);
} 


/*tabela do estádio*/
let estadios = [];
class Estadio {
  constructor(nome, localizacao, capacidade, proprietario) {
    this.nome = nome;
    this.localizacao = localizacao;
    this.capacidade = capacidade;
    this.proprietario = proprietario;
  }
}

function infoEstadio() {
  let nome = byId("nomeEstadio").value;
  let localizacao = byId("localizacao").value;
  let capacidade = byId("capacidade").value;
  let proprietario = byId("proprietario").value;

  let estadio= new Estadio(nome, localizacao, capacidade, proprietario);
  estadios.push(estadio);
  apresentarEstadios();
}

function apresentarEstadios() {
  let tabela2 = byId("tabela2");
  let listagem  = "<ol>"
  for(let estadio of estadios) {
    listagem += `<li> ${estadio.nome}
       [${estadio.localizacao},
        ${estadio.capacidade},
        ${estadio.proprietario}]</li>`;
    }
  listagem += "</ol>" 
  tabela2.innerHTML = listagem;
}

function byId(id) {
  return document.getElementById(id);
}

/*tabela do tecnico*/

let tecnicos = [];
class Tecnico {
  constructor(nome, dataDeNascimentoTec, idDoTecnico, timeTecnico) {
    this.nome = nome;
    this.dataDeNascimentoTec = dataDeNascimentoTec;
    this.idDoTecnico = idDoTecnico;
    this.timeTecnico = timeTecnico;
  }
}

function infoTecnico() {
  let nome = byId("nomeTecnico").value;
  let dataDeNascimentoTec = byId("dataDeNascimentoTec").value;
  let idDoTecnico = byId("idDoTecnico").value;
  let timeTecnico = byId("timeTecnico").value;

  let tecnico = new Tecnico(nome, dataDeNascimentoTec, idDoTecnico, timeTecnico);
  tecnicos.push(tecnico);
  apresentarTecnico();
}

function apresentarTecnico() {
  let tabela3 = byId("tabela3");
  let listagem  = "<ol>"
  for(let tecnico of tecnicos) {
    listagem += `<li> ${tecnico.nome}
       [${tecnico.dataDeNascimentoTec},
        ${tecnico.idDoTecnico},
        ${tecnico.timeTecnico}]</li>`;
    }
  listagem += "</ol>" 
  tabela3.innerHTML = listagem;
}

function byId(id) {
  return document.getElementById(id);
}

/*tabela p os jogadores*/

let jogadores = [];
class Jogador {
  constructor(nomeJog, dataJog, posicao, numeracao, idJog) {
    this.nomeJog = nomeJog;
    this.dataJog= dataJog;
    this.posicao = posicao;
    this.numeracao = numeracao;
    this.idJog = idJog;
  }
}

function infoJog() {
  let nomeJog = byId("nomeJog").value;
  let dataJog = byId("dataJog").value;
  let posicao = byId("posicao").value;
  let numeracao = byId("numeracao").value;
  let idJog = byId("idJog").value;

  let jogador = new Jogador(nomeJog, dataJog, posicao, numeracao, idJog);
  jogadores.push(jogador);
  apresentarJog();
}

function apresentarJog() {
  let tabela5 = byId("tabela5");
  let listagem  = "<ol>"
  for(let jogador of jogadores) {
    listagem += `<li> ${jogador.nomeJog}
       [${jogador.dataJog},
        ${jogador.posicao},
        ${jogador.numeracao}
        ${jogador.idJog}]</li>`;
    }
  listagem += "</ol>" 
  tabela5.innerHTML = listagem;
}

function byId(id) {
  return document.getElementById(id);
}

/*tabela arbitros*/

let arbitros = [];
class Arbitro {
  constructor(nomeArbitro,dataDeNascimentoArb, nivel, entidade){
    this.nomeArbitro = nomeArbitro;
    this.dataDeNascimentoArb = dataDeNascimentoArb;
    this.nivel = nivel;
    this.entidade = entidade;
   }
}

function infoArbitro() {
  let nomeArbitro = byId("nomeArbitro").value;
  let dataDeNascimentoArb = byId("dataDeNascimentoArb").value;
  let nivel = byId("nivel").value;
  let entidade = byId("entidade").value;

  let arbitro = new Arbitro(nomeArbitro, dataDeNascimentoArb, nivel, entidade);
  arbitros.push(arbitro);
  apresentarArbitros();
}

function apresentarArbitros() {
  let tabela4 = byId("tabela4");
  let listagem  = "<ol>"
  for(let arbitro of arbitros) {
    listagem += `<li> ${arbitro.nomeArbitro}
       [${arbitro.dataDeNascimentoArb},
        ${arbitro.nivel},
        ${arbitro.entidade}]</li>`;
    }
  listagem += "</ol>" 
  tabela4.innerHTML = listagem;
}

function byId(id) {
  return document.getElementById(id);
}

