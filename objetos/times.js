import {Time} from "./classes/cdb.js";
let time1 = new Time("Ituano", "Itu-SP", 456789, 0);
console.log(time1);

import {Time} from "./classes/cdb.js";
let time2 = new Time("São Paulo", "São Paulo-SP", 678901, 0);
console.log(time2);

import {Time} from "./classes/cdb.js";
let time3 = new Time("Volta Redonda", "Volta Redonda-RJ", 987098, 0);
console.log(time3);

import {Time} from "./classes/cdb.js";
let time4 = new Time("Bahia", "Salvador-BA", 976543, 0);
console.log(time4);

import {Time} from "./classes/cdb.js";
let time5 = new Time("Botafogo-SP", "Ribeirão Preto- SP", 987654, 0);
console.log(time5);

import {Time} from "./classes/cdb.js";
let time6 = new Time("Santos", "Santos-SP", 876509, 1);
console.log(time6);

import {Time} from "./classes/cdb.js";
let time7 = new Time("Internacional", "Porto Alegre- RS", 678909, 1);
console.log(time7);

import {Time} from "./classes/cdb.js";
let time8 = new Time("CSA", "Maceió- AL", 876543, 0);
console.log(time8);

import {Time} from "./classes/cdb.js";
let time9 = new Time("Fortaleza", "Fortaleza-CE", 098745, 0);
console.log(time9);

import {Time} from "./classes/cdb.js";
let time10 = new Time("Águia de Marabá", "Marabá-PR", 987654, 0);
console.log(time10);
