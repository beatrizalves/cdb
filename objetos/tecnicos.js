import {Tecnico} from "./cdb.js";
let tecnico1 = new Tecnico("Gilmar Dell Pozo", "14/08/1962", 245678, "Ituano-SP");
console.log(tecnico1);

import {Tecnico} from "./cdb.js";
let tecnico2 = new Tecnico("Dorival Junior", "12/01/1964", 125689, "São Paulo FC");
console.log(tecnico2);

import {Tecnico} from "./classes/cdb.js";
let tecnico3 = new Tecnico("Rogerio Corrêa de Oliveira", "08/04/1952", 248765, "Volta Redonda");
console.log(tecnico3);

import {Tecnico} from "./classes/cdb.js";
let tecnico4 = new Tecnico("Renato Paiva", "10/06/1960", 249678, "Bahia");
console.log(tecnico4);

import {Tecnico} from "./classes/cdb.js";
let tecnico5 = new Tecnico("Roberto Cavalo", "03/01/1952", 2456000, "Botafogo-SP");
console.log(tecnico5);

import {Tecnico} from "./classes/cdb.js";
let tecnico6 = new Tecnico("Odair Helmann", "23/12/1965", 245988, "Santos");
console.log(tecnico6);

import {Tecnico} from "./classes/cdb.js";
let tecnico7 = new Tecnico("Mano Menezes", "06/09/1958", 243098, "Internacional de Porto Alegre");
console.log(tecnico7);

import {Tecnico} from "./classes/cdb.js";
let tecnico8 = new Tecnico("José Alberto Moraes", "25/11/1968", 242078, "CSA");
console.log(tecnico8);

import {Tecnico} from "./classes/cdb.js";
let tecnico9 = new Tecnico("Juan Pablo Vojvoda", "10/07/1959", 248765, "Fortaleza");
console.log(tecnico9);

import {Tecnico} from "./classes/cdb.js";
let tecnico10 = new Tecnico("Matheus Sodré", "15/08/1970", 249978, "Águia de Marabá");
console.log(tecnico10);