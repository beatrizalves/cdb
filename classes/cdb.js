export class Jogoida{
    constructor(id, visitante, mandante, jogadores, gols, local, publico){
        this.id = id;
        this.visitante = visitante;
        this.mandante = mandante;
        this.jogadores = jogadores;
        this.gols = gols;
        this.local = local;
        this.publico = publico;
}
    imprimirDados(){
    console.log(`      
                 ${this.id}
                 ${this.visitante}
                 ${this.mandante}
                 ${this.jogadores}
                 ${this.gols}
                 ${this.local}
                 ${this.publico}`)
}
          
}
let ida = new Jogo(4567, "Ituano", "São Paulo", 0, 0, "Morumbi", 20.000);
jogoIda.imprimirDados();

export class Time{
    constructor(nome,cidadeSede,id, titulosNaCompeticao){
        this.nome = nome;
        this.cidadeSede = cidadeSede;
        this.id = id;
        this.titulosNaCompeticao = titulosNaCompeticao;
    }
    imprimirDados1(){
        console.log(`      
                     ${this.nome}
                     ${this.cidadeSede}
                     ${this.id}
                     ${this.titulosNaCompeticao}`)
    }
}
let time = new Time("São Paulo", "São Paulo-SP", 123456, 0);
console.log(time);


export class Estadio{
    constructor(localizacao, capacidade, nome, proprietario){
        this.localizacao = localizacao;
        this.capacidade = capacidade;
        this.nome = nome;
        this.proprietario = proprietario;
    }
    imprimirDados2(){
        console.log(`
                     ${this.localizacao}
                     ${this.capacidade}
                     ${this.proprietario}
                     ${this.nome}`)
    }
}
let estadio = new Estadio("São Paulo, Bairro do Morumbi", "66.795", "Cícero Pompeo De Toledo", "São Paulo FC");
estadio.imprimirDados2();

export class Jogador{
    constructor(nome, dataNasc, posicao, numero, id){
        this.nome = nome;
        this.dataNasc = dataNasc;
        this.posicao = posicao;
        this.numero = numero;
        this.id = id;
    }
    imprimirDados3(){
        console.log(`
                      ${this.nome}
                      ${this.dataNasc}
                      ${this.posicao}
                      ${this.numero}
                      ${this.id}`)
    }
}
let jogador = new Jogador("Calleri", "20/05", "centro-avante", "9", "121314");
console.log(jogador);


export class Arbitro{
    constructor(nome, dataDenasc, nivel, entidade){
        this.nome = nome;
        this.dataDenasc = dataDenasc;
        this.nivel = nivel;
        this.entidade = entidade;
       }
imprimirDados5(){
    console.log(`${this.nome}
                 ${this.dataDenasc}
                 ${this.nivel}
                 ${this.entidade}`
    )
}
}

let arbitro = new Arbitro("Daronco", "17/02/1978", "4", "CBF");
console.log(arbitro);

export class Tecnico{
    constructor(nome, DataDeNasc, id, time){
        this.nome = nome;
        this.DataDeNasc = DataDeNasc;
        this.id = id;
        this.time = time;
    }    
    imprimirInformacoes(){
        console.log(`
                      ${this.nome}
                      ${this.DataDeNasc}
                      ${this.id}
                      ${this.time}`)
    }
}
let tecnico = new Tecnico("Dorival", "12/01/1964", 125689, "São Paulo");
console.log(tecnico);





    